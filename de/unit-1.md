# Kurs laden

Kurse können über einen Schulserver oder aus einer Textdatei via URL geladen werden. für letzteres wird hier Github verwendet, mit dem die Kurse auch gleich versioniert und von mehreren Autoren im Netz bearbeitet werden können.

## Laden von Kursen von einem Schulserver

* Schule anlegen: unter Einstellungen -&gt; Schulen -&gt; Bearbeiten -&gt; Schule hinzufügen \(Wenn du noch keine Schule hast, kannst du zunächst meine Beispielschule ausprobieren unter [http://tala-example.ddns.net/tala](http://tala-example.ddns.net/tala) \)
* Nach Eingabe der Schul-URL und Bestätigung mit Enter werden die Informationen zur Schule geladen \(wenn keine Informationen geladen werden - URL überprüfen\)
* Schule hinzufügen und zurück zum Menü
* Kategorie erstellen und auswählen
* Kategorie bearbeiten -&gt; Neuer Kurs -&gt; Kurs abonnieren
* Schule auswählen - die verfügbaren Kurse werden geladen
* Kurse auswählen und hinzufügen

$$1 = 2^0$$

*Link der im Web funktioniert (relativ)*
![Example from GitBook](/gitbook-auswahl.png)

*Link der im Tablet funktioniert (remote)*
![Example from GitBook](https://raw.githubusercontent.com/axel-klinger/tala-tutorial/master/gitbook-example.png)


## Laden von Kursen via Github

* Kategorie anlegen -&gt; Bearbeiten -&gt; Neuer Kurs -&gt; Kurs laden
* Beispiel-Kurs: [https://raw.githubusercontent.com/axel-klinger/tala-tutorial/master/xy/course.md](https://raw.githubusercontent.com/axel-klinger/tala-tutorial/master/xy/course.md)



