Einführung in TALA
---
## TALA - Eine Einführung

Mit TALA können Bildungsmaterialien zu Kursen zusammengefasst, geteilt und geladen werden.

---
## [Kurse laden](unit-1.md)
Kurse können von einem Schul-Server oder als Textdatei von GitHub geladen werden. Letzteres bietet sich für die Erstellung und Versionierung von Kursen im Web an.

## [Kurse erstellen](unit-4.md)
Es braucht nicht viel, um einen Kurs zu erstellen. Einfache Texte lassen sich bereits mit Handy oder Tablet erstellen. Zum Erstellen von Videos reichen schon Papier, Stift und Handy oder eine einfache App wie ExplainEverything.

## [Kurse bereitstellen](unit-2.md)
Kurse können über Schul-Server oder auf GitHub bereit gestellt werden. Schul-Server können von Schulen oder gemeinsamen Bildungsservern betrieben werden und erfordern eine Registrierung zum Bereitstellen von Kursen. Auf GitHub kann man sich ebenfallse registrieren, um Kurse zu erstellen, zu versionieren und im Netz bereit zu stellen.

## [Beispiele für Inhalte](unit-3.md)
Kurzanleitung für das Textformat Markdown. Eine einfache Sprache zur Formatierung von Texten mit Überschriften, Absätzen, Links, Bildern, Listen, Videos und Aufgaben.
