
### Was ist TALA?

TALA (Teach And Learn Anything) ist ein freier Zugang zu Bildung - und da es evolutionär ist, zukünftig zur besten Bildung!

* ermöglicht es jedem das zu lernen, was er/sie braucht bzw. lernen will oder lernen sollte
* ist eine Plattform für OER (Open Education Resources = Freie Bildungsinhalte/-Materialien)

### Wie funktioniert TALA?

Schulen und andere staatliche Bildungseinrichtungen stellen ihre Inhalte der Allgemeinheit zur Verfügung und jeder kann die Kurse frei und ohne Registrierung abonnieren.

* mobile App für Smartphone und vor allem Tablet für alle persönlichen Bildungsinhalte
* ein Server pro Schule mit allen Kursen (leicht gewichtig, nur text)
* in Arbeit ist eine zentrale Registrierung aller Schulen für die Suche nach passenden/besten Kursen

* bietet die Möglichkeit alle Inhalte zum Bestehen aller Kurse im aktuellen Jahrgang bereit zu stellen
* Inhalte, Aufgaben oder Präsentationen können von allen Lehrern, Schülern, Eltern etc. erweitert und verbessert werden
* ermöglicht es Kurse an anderen Schulen zu lernen

### Was kann TALA lösen?

Was mich interessiert ...

* gibt es nicht in meinem Jahrgang
* gibt es nicht in meinem Schultyp
* wird an meiner Schule nicht angeboten
* kann ich aufgrund von Wartelisten nicht gleich lernen
* gibt es erst in Uni/Ausbildung
* kann ich aufgrund meines Zeugnisses erst in X Jahren anfangen
* kann ich mir nicht leisten zu lernen
* habe ich verpasst
* wurde durch Ausfall der Stunden nicht angeboten

### Häufige gestellte Fragen

Wenn ich etwas schreibe, ist es dann gleich online?

* Nein, alles was ich erstelle ist zunächst lokal auf meinem Gerät gespeichert. Um es zu veröffentlichen muss ich erst einen Autorenzugang auf einem Server haben und den Kurs explizit veröffentlichen.

Muss ich mich irgendwo mit meinen persönlichen Daten registrieren?

* Als Schüler/Student brauche ich micht nicht zu registrieren um Kurse zu laden. Als Lehrer brauche ich einen Zugang mit Schreibrechten auf einem Server mit Name und Email Adresse.
