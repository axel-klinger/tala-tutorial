
Mathe - Terme
---
## Term

Wikipedia: In der Mathematik bezeichnet ein Term einen sinnvollen Ausdruck, der Zahlen, Variablen, Symbole für mathematische Verknüpfungen und Klammern enthalten kann. Terme sind die syntaktisch korrekt gebildeten Wörter oder Wortgruppen in der formalen Sprache der Mathematik.

In der Praxis wird der Begriff häufig benutzt, um über einzelne Bestandteile einer Formel oder eines größeren Terms zu reden.

---
## [Theorie](theorie.md)
## [Aufgaben](aufgaben.md)
