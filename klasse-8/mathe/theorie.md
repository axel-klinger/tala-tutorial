## Terme

### Umgangssprachlich

Der Begriff „Term“ wird umgangssprachlich für alles verwendet, das eine Bedeutung trägt. Im engeren Sinn sind mathematische Gebilde gemeint, die man prinzipiell ausrechnen kann, zumindest wenn man den darin enthaltenen Variablen Werte zugewiesen hat. So ist zum Beispiel (x+y)^2 ein Term, denn weist man den darin enthaltenen Variablen x und y einen Wert zu, so erhält auch der Term einen Wert. Statt Zahlen können hier auch andere Werte in Betracht kommen, so ist etwa ...

Grob kann man sagen, dass ein Term eine Seite einer Gleichung oder Relation, z.B. einer Ungleichung, ist. Die Gleichung oder Relation selbst ist kein Term, sie besteht aus Termen.
